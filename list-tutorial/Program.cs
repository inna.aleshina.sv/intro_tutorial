﻿using System;
using System.Collections.Generic;

namespace list_tutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkingWithStrings();
            Console.WriteLine("Try to write the code to generate the first 20 numbers in the sequence. (As a hint, the 20th Fibonacci number is 6765)");
            var fibonacciNumbers = new List<int> { 1, 1 };

            while (fibonacciNumbers.Count < 20)
            {
                var previous = fibonacciNumbers[fibonacciNumbers.Count - 1];
                var previous2 = fibonacciNumbers[fibonacciNumbers.Count - 2];
                fibonacciNumbers.Add(previous + previous2);
            }

            foreach (var item in fibonacciNumbers)
                Console.WriteLine(item);
        }



        public static void WorkingWithStrings()
        {
            Console.WriteLine("String Array");
            var names = new List<string> { "Inna", "Ana", "Felipe" };
            foreach (var name in names)
            {
                Console.WriteLine($"Hello {name.ToUpper()}!");
            }

            Console.WriteLine();
            names.Add("Maria");
            names.Add("Bill");
            names.Remove("Ana");
            foreach (var name in names)
            {
                Console.WriteLine($"Hello {name.ToUpper()}!");
            }
            Console.WriteLine("----------------------------------------------");

            Console.WriteLine("Playing with indexing and count");
            Console.WriteLine($"My name is {names[0]}");
            Console.WriteLine($"I've added {names[2]} and {names[3]} to the list");

            Console.WriteLine($"The list has {names.Count} people in it");
           var index = names.IndexOf("Felipe");
            Console.WriteLine($"The name {names[index]} is at index {index}");

            var notFound = names.IndexOf("Not Found");
            Console.WriteLine($"When an item is not found, IndexOf returns {notFound}");

            names.Sort();
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine("Sorting of the list");
            foreach (var name in names)
            {
                Console.WriteLine($"Hello {name.ToUpper()}!");
               

            }
            Console.WriteLine("----------------------------------------------");
        }
    }
}