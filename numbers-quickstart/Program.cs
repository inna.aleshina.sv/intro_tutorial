﻿using System;

namespace NumbersInCSharp
{
    class Program
    {
        static void WorkingWithIntegers()
        {
            Console.WriteLine($"working with Integers");
            int a = 18;
            Console.WriteLine($"a = {a}");

            int b = 6;
            Console.WriteLine($"b = {b}");
            int c = a + b;
            Console.WriteLine($"c = a + b = {c}");
            c = a - b;
            Console.WriteLine($"c = a - b = {c}");
            c = a * b;
            Console.WriteLine($"c = a * b = {c}");
            c = a / b;
            Console.WriteLine($"c = a / b = {c}");
            Console.WriteLine($"-----------------------------------------------------------------");
        }

        static void OrderPrecedence()
        {
            Console.WriteLine($"Integers, playing with order");
            int a = 5;
            Console.WriteLine($"a = {a}");

            int b = 4;
            Console.WriteLine($"b = {b}");

            int c = 2;
            Console.WriteLine($"c = {c}");

            int d = a + b * c;
            Console.WriteLine($"d = a + b * c  = {d}");

            d = (a + b) * c;
            Console.WriteLine($"d = (a + b) * c  = {d}");

            d = (a + b) - 6 * c + (12 * 4) / 3 + 12;
            Console.WriteLine($"d = (a + b) - 6 * c + (12 * 4) / 3 + 12 = {d}");
            Console.WriteLine($"-----------------------------------------------------------------");



            int e = 7;
            Console.WriteLine($"e = {e}");

            int f = 4;
            Console.WriteLine($"f = {f}");

            int g = 3;
            Console.WriteLine($"g = {g}");

            int h = (e + f) / g;
            Console.WriteLine($"h = (e + f) / g = {h}");

            int a1 = 7;
            Console.WriteLine($"a1 = {a1}");

            int b1 = 4;
            Console.WriteLine($"b1 = {b1}");

            int c1 = 3;
            Console.WriteLine($"c1 = {c1}");

            int d1 = (a1 + b1) / c1;
            Console.WriteLine($"quotient > d1 = (a1 + b1) / c1 = {d1}");

            int e1 = (a1 + b1) % c1;
            Console.WriteLine($"remainder > for e1 = (a1 + b1) % c1 = {e1}");
            Console.WriteLine($"-----------------------------------------------------------------");


        }
        static void WorkingDoubles()
        {
            Console.WriteLine($"Working with Doubles");
            double a = 5;
            Console.WriteLine($"a = {a}");

            double b = 4;
            Console.WriteLine($"b = {b}");

            double c = 2;
            Console.WriteLine($"c = {c}");

            double d = (a + b) / c;
            Console.WriteLine($"d = (a + b) / c = {d}");

            d = (a/a + b-b) / c;
            Console.WriteLine($"d = (a/a + b-b) / c = {d}");

            d = (a - a + b - b) / c;
            Console.WriteLine($"d = (a - a + b - b) / c = {d}");
            Console.WriteLine($"-----------------------------------------------------------------");

            double e = 1.797693134862;
            Console.WriteLine($"e = {e}");

            double f = 23;
            Console.WriteLine($"f = {f}");

            double g = 8;
            Console.WriteLine($"g = {g}");

            double h = (e*100000000 + f) / g;
            Console.WriteLine($"h = (e*100000000 + f) / g = {h}");
            Console.WriteLine($"-----------------------------------------------------------------");
        }

        static void WorkingDecimals()
        {
            Console.WriteLine($"Working with Decimals");
            decimal c = 1.0M;
            Console.WriteLine($"c = {c}");

            decimal d = 3.0M;
            Console.WriteLine($"d = {d}");

            Console.WriteLine($"c / d = {c / d}");
            Console.WriteLine($"c * d = {c * d}");
            Console.WriteLine($"-----------------------------------------------------------------");

        }

        static void CalculatingCircleArea()
        {
            Console.WriteLine($"Calculating_Circle_Area");
            double radius = 0.25;
            Console.WriteLine($"radius is = { radius }");
            Console.WriteLine($"Circle_Area = pi *(r)^2 = {(Math.PI)*radius*radius }");
            Console.WriteLine($"-----------------------------------------------------------------");

        }

        static void Main(string[] args)
        {
            WorkingWithIntegers();

            OrderPrecedence();
          //  int max = int.MaxValue;
         //   int min = int.MinValue;
          //  Console.WriteLine($"The range of integers is {min} to {max}");

            WorkingDoubles();
          //  double max1 = double.MaxValue;
         //   double min1 = double.MinValue;
          //  Console.WriteLine($"The range of double is {min1} to {max1}");

            WorkingDecimals();
         //   decimal min2 = decimal.MinValue;
        //   decimal max2 = decimal.MaxValue;
         //   Console.WriteLine($"The range of the decimal type is {min2} to {max2}");

            CalculatingCircleArea();
            
        }
    }
}