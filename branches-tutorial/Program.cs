﻿using System;

namespace BranchesAndLoops
{
    class Program
    {
        static void ExploreIf()
        {
            // int a = 5;
            // Console.WriteLine($"a = {a}");

            //  int b = 6;
            //  Console.WriteLine($"b = {b}");

            //  int b1 = 3;
            //  Console.WriteLine($"b1 = {b1}");

            //  if (a + b > 10)
            //  Console.WriteLine("Is (a + b) > 10  ?");
            //  Console.WriteLine($"a + b = {a+b} and that is > 10");

            //  if (a + b1 < 10)
            //      Console.WriteLine("Is (a + b1) < 10  ?");
            //  Console.WriteLine($"a + b1 = {a + b1} and that is < 10");

            Console.WriteLine("IF operations");

            int a = 5;
            Console.WriteLine($"a = {a}");

            int b = 3;
            Console.WriteLine($"b = {b}");

            int c = 4;
            Console.WriteLine($"c = {c}");

            Console.WriteLine("Is the  (a + b + c > 10) && (a == b)  ? ");

            if ((a + b + c > 10) && (a == b))
            {
                
                Console.WriteLine("The answer YES");
                Console.WriteLine(" a + b + c is greater than 10");
                Console.WriteLine("or the first number is equal to the second");
            }
            else
            {
                Console.WriteLine("The answer is NO");
                Console.WriteLine("a + b + c is not greater than 10");
                Console.WriteLine("Or the first number is not equal to the second");
            }

            Console.WriteLine("---------------------------------------------------");

            int a1 = 2;
            Console.WriteLine($"a1 = {a1}");

            int b1 = 1;
            Console.WriteLine($"b1 = {b1}");

            int c1 = 4;
            Console.WriteLine($"c1 = {c1}");

            Console.WriteLine("Is the  (a1 + b1 + c1 > 10) || (a == b)  ? ");



            if ((a1 + b1 + c1 > 10) || (a1 == b1))
            {

                Console.WriteLine("The answer YES");
                Console.WriteLine(" a + b + c is greater than 10");
                Console.WriteLine("or the first number is equal to the second");
            }
            else
            {
                Console.WriteLine("The answer is NO");
                Console.WriteLine("a + b + c is not greater than 10");
                Console.WriteLine("Or the first number is not equal to the second");

            }
            Console.WriteLine("---------------------------------------------------");
        }

                       static void Loops()
        {
            Console.WriteLine("Loop operations");
            Console.WriteLine("while do");
            int counter = 0;
            while (counter < 10)
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
                counter++;
            }
            Console.WriteLine("---------------------------------------------------");

            counter = 0;
            Console.WriteLine("do while");
            do
            {
                Console.WriteLine($"Hello World! The counter is {counter}");
                counter++;
            } while (counter < 10);
            Console.WriteLine("---------------------------------------------------");

            Console.WriteLine("for loop");
            for (int index = 2; index < 7; index++)
            {
                Console.WriteLine($"Hello World! The index is {index}");
            }
            Console.WriteLine("---------------------------------------------------");
        }


        static void Task()
        {
            int number =3;
            Console.WriteLine($"TASK: Find all all integers  from 1 to 20 that are divisible by  {number}, Sum them up. Get the answer");
            int sum = 0;
            
            for (int index = 1; index <= 20; index++)
               
            {
               
                if (index % number == 0)
                            {
                    Console.WriteLine($" The number {index} can be devided by {number} without reminder");
                    sum = sum + index;
                   
                }
                
            }
           
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine($"Total sum of all numbers that can be devided by {number} without remainder is: {sum} ");
        }


        static void Main(string[] args)
        {
            ExploreIf();
            Loops();
            Task( );

        }
    }
}
